package interview.chess.ui;

import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Borys Zora
 * @version 2020-03-10
 */
class AlgebraicNotationGameReaderTest {

    @Test
    void gameKasparovDorfman() throws Exception {
        String gameRecording = "Kasparov-Dorfman-1978.txt";
        AlgebraicNotationGameReader game = new AlgebraicNotationGameReader(gameRecording);
        game.start();
        assertThat(game.totalMoves(), Is.is(78));
    }

    @Test
    void gameKasparovSuba() throws Exception {
        String gameRecording = "Kasparov-Suba-1982-round-12.txt";
        AlgebraicNotationGameReader game = new AlgebraicNotationGameReader(gameRecording);
        game.start();
        assertThat(game.totalMoves(), Is.is(96));
    }

    @Test
    void gameDominguezKasparov() throws Exception {
        String gameRecording = "Dominguez-Kasparov-2017-08-18-round-17.1.txt";
        AlgebraicNotationGameReader game = new AlgebraicNotationGameReader(gameRecording);
        game.start();
        assertThat(game.totalMoves(), Is.is(97));
    }

}