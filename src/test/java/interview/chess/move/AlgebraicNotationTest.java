package interview.chess.move;

import interview.chess.game.Board;
import interview.chess.game.NewGameSetup;
import interview.chess.game.Player;
import interview.chess.model.*;
import interview.chess.move.exception.MoveException;
import interview.chess.ui.BoardView;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static interview.chess.model.Color.BLACK;
import static interview.chess.model.Color.WHITE;
import static interview.chess.move.Coordinate.of;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Borys Zora
 * @version 2020-03-05
 */
class AlgebraicNotationTest {

    AlgebraicNotation notation = new AlgebraicNotation();
    Board board;
    Player whitePlayer;
    Player blackPlayer;
    BoardView view = new BoardView();

    @BeforeEach
    void setUp() {
        board = new Board();
        new NewGameSetup(board).setup();
        whitePlayer = new Player(board, WHITE);
        blackPlayer = new Player(board, Color.BLACK);
        view.doDraw(board);
    }

    // Nfxd6
    // Nef7+
    // Rfe8
    // Qh4e1
    // Qh4xe1+
    @Test
    void parseScenarios() throws Exception {

        assertThrows(MoveException.class, () -> {
            Move d5 = notation.parse("d5", WHITE, board);
            assertThat(d5.motivation(), is(MoveMotivation.PEACE));
            assertThat(d5.type(), is(MoveType.TWO_SQUARE_FORWARD_FILE_ONLY_FIRST_MOVE));
            assertThat(d5.origin(), is(of("d2")));
            blackPlayer.movePiece(d5);
            print(d5);
            view.doDraw(board);
        });

        Move e4 = notation.parse("e4", WHITE, board);
        assertThat(e4.motivation(), is(MoveMotivation.PEACE));
        assertThat(e4.type(), is(MoveType.TWO_SQUARE_FORWARD_FILE_ONLY_FIRST_MOVE));
        assertThat(e4.origin(), is(of("e2")));
        whitePlayer.movePiece(e4);
        print(e4);
        view.doDraw(board);

        Move e5 = notation.parse("e5", BLACK, board);
        assertThat(e5.motivation(), is(MoveMotivation.PEACE));
        assertThat(e5.type(), is(MoveType.TWO_SQUARE_FORWARD_FILE_ONLY_FIRST_MOVE));
        assertThat(e5.origin(), is(of("e7")));
        blackPlayer.movePiece(e5);
        print(e5);
        view.doDraw(board);

        Move d4 = notation.parse("d4", WHITE, board);
        assertThat(d4.motivation(), is(MoveMotivation.PEACE));
        assertThat(d4.type(), is(MoveType.TWO_SQUARE_FORWARD_FILE_ONLY_FIRST_MOVE));
        assertThat(d4.origin(), is(of("d2")));
        whitePlayer.movePiece(d4);
        print(d4);
        view.doDraw(board);

        Move exd4 = notation.parse("exd4", BLACK, board);
        assertThat(exd4.motivation(), is(MoveMotivation.ATTACK));
        assertThat(exd4.type(), is(MoveType.ONE_SQUARE_FORWARD_DIAGONAL_ATTACK_ONLY));
        assertThat(exd4.origin(), is(of("e5")));
        blackPlayer.movePiece(exd4);
        print(exd4);
        view.doDraw(board);

        Move qxd4 = notation.parse("Qxd4", WHITE, board);
        assertThat(qxd4.motivation(), is(MoveMotivation.ATTACK));
        assertThat(qxd4.type(), is(MoveType.FILE));
        assertThat(qxd4.origin(), is(of("d1")));
        whitePlayer.movePiece(qxd4);
        print(qxd4);
        view.doDraw(board);

        Move nc6 = notation.parse("Nc6", BLACK, board);
        assertThat(nc6.motivation(), is(MoveMotivation.PEACE));
        assertThat(nc6.type(), is(MoveType.SECOND_SQUARE_NEIGHBORS_OPPOSITE_COLOR));
        assertThat(nc6.origin(), is(of("b8")));
        blackPlayer.movePiece(nc6);
        print(nc6);
        view.doDraw(board);

        Move bb5 = notation.parse("Bb5", WHITE, board);
        assertThat(bb5.motivation(), is(MoveMotivation.PEACE));
        assertThat(bb5.type(), is(MoveType.DIAGONAL));
        assertThat(bb5.origin(), is(of("f1")));
        whitePlayer.movePiece(bb5);
        print(bb5);
        view.doDraw(board);

        Move a6 = notation.parse("a6", BLACK, board);
        assertThat(a6.motivation(), is(MoveMotivation.PEACE));
        assertThat(a6.type(), is(MoveType.ONE_SQUARE_FORWARD_FILE));
        assertThat(a6.origin(), is(of("a7")));
        blackPlayer.movePiece(a6);
        print(a6);
        view.doDraw(board);

        assertThrows(MoveException.class, () -> {
            Move bxe8 = notation.parse("Bxe8", WHITE, board);
            whitePlayer.movePiece(bxe8);
            print(bxe8);
            view.doDraw(board);
        });

        Move nf3 = notation.parse("Nf3", WHITE, board);
        assertThat(nf3.motivation(), is(MoveMotivation.PEACE));
        assertThat(nf3.type(), is(MoveType.SECOND_SQUARE_NEIGHBORS_OPPOSITE_COLOR));
        assertThat(nf3.origin(), is(of("g1")));
        whitePlayer.movePiece(nf3);
        print(nf3);
        view.doDraw(board);

        Move nxd4 = notation.parse("Nxd4", BLACK, board);
        assertThat(nxd4.motivation(), is(MoveMotivation.ATTACK));
        assertThat(nxd4.type(), is(MoveType.SECOND_SQUARE_NEIGHBORS_OPPOSITE_COLOR));
        assertThat(nxd4.origin(), is(of("c6")));
        blackPlayer.movePiece(nxd4);
        print(nxd4);
        view.doDraw(board);

        Move castling = notation.parse("O-O", WHITE, board);
        assertThat(castling.motivation(), is(MoveMotivation.PEACE));
        assertThat(castling.type(), is(MoveType.CASTLING_KING_SIDE));
        assertThat(castling.origin(), is(of("e1")));
        whitePlayer.movePiece(castling);
        assertThat(board.getPieceByCoordinate(of("g1")).getClass(), is(King.class));
        assertThat(board.getPieceByCoordinate(of("f1")).getClass(), is(Rook.class));
        print(castling);
        view.doDraw(board);

        Move qh4 = notation.parse("Qh4", BLACK, board);
        assertThat(qh4.motivation(), is(MoveMotivation.PEACE));
        assertThat(qh4.type(), is(MoveType.DIAGONAL));
        assertThat(qh4.origin(), is(of("d8")));
        blackPlayer.movePiece(qh4);
        print(qh4);
        view.doDraw(board);

        Move e51 = notation.parse("e5", WHITE, board);
        assertThat(e51.motivation(), is(MoveMotivation.PEACE));
        assertThat(e51.type(), is(MoveType.ONE_SQUARE_FORWARD_FILE));
        assertThat(e51.origin(), is(of("e4")));
        whitePlayer.movePiece(e51);
        print(e51);
        view.doDraw(board);

        assertThrows(MoveException.class, () -> {
            Move d5 = notation.parse("d5", BLACK, board);
            assertThat(d5.motivation(), is(MoveMotivation.PEACE));
            assertThat(d5.type(), is(MoveType.TWO_SQUARE_FORWARD_FILE_ONLY_FIRST_MOVE));
            assertThat(d5.origin(), is(of("d7")));
            blackPlayer.movePiece(d5);
            print(d5);
            view.doDraw(board);
        });

        Move axb5 = notation.parse("axb5", BLACK, board);
        assertThat(axb5.motivation(), is(MoveMotivation.ATTACK));
        assertThat(axb5.type(), is(MoveType.ONE_SQUARE_FORWARD_DIAGONAL_ATTACK_ONLY));
        assertThat(axb5.origin(), is(of("a6")));
        blackPlayer.movePiece(axb5);
        print(axb5);
        view.doDraw(board);

        Move a4 = notation.parse("a4", WHITE, board);
        assertThat(a4.motivation(), is(MoveMotivation.PEACE));
        assertThat(a4.type(), is(MoveType.TWO_SQUARE_FORWARD_FILE_ONLY_FIRST_MOVE));
        assertThat(a4.origin(), is(of("a2")));
        whitePlayer.movePiece(a4);
        print(a4);
        view.doDraw(board);

        Move d5 = notation.parse("d5", BLACK, board);
        assertThat(d5.motivation(), is(MoveMotivation.PEACE));
        assertThat(d5.type(), is(MoveType.TWO_SQUARE_FORWARD_FILE_ONLY_FIRST_MOVE));
        assertThat(d5.origin(), is(of("d7")));
        blackPlayer.movePiece(d5);
        print(d5);
        view.doDraw(board);

        Move exd6 = notation.parse("exd6", WHITE, board);
        assertThat(exd6.motivation(), is(MoveMotivation.ATTACK));
        assertThat(exd6.type(), is(MoveType.EN_PASSANT));
        assertThat(exd6.origin(), is(of("e5")));
        whitePlayer.movePiece(exd6);
        print(exd6);
        view.doDraw(board);

        Move bg4 = notation.parse("Bg4", BLACK, board);
        assertThat(bg4.motivation(), is(MoveMotivation.PEACE));
        assertThat(bg4.type(), is(MoveType.DIAGONAL));
        assertThat(bg4.origin(), is(of("c8")));
        blackPlayer.movePiece(bg4);
        print(bg4);
        view.doDraw(board);

        Move dxc7 = notation.parse("dxc7", WHITE, board);
        assertThat(dxc7.motivation(), is(MoveMotivation.ATTACK));
        assertThat(dxc7.type(), is(MoveType.ONE_SQUARE_FORWARD_DIAGONAL_ATTACK_ONLY));
        assertThat(dxc7.origin(), is(of("d6")));
        whitePlayer.movePiece(dxc7);
        print(dxc7);
        view.doDraw(board);

        assertThrows(MoveException.class, () -> {
            Move castlingQueenSide = notation.parse("O-O-O", BLACK, board);
            assertThat(castlingQueenSide.motivation(), is(MoveMotivation.PEACE));
            assertThat(castlingQueenSide.type(), is(MoveType.CASTLING_QUEEN_SIDE));
            assertThat(castlingQueenSide.origin(), is(of("e8")));
            blackPlayer.movePiece(castlingQueenSide);
            print(castlingQueenSide);
            view.doDraw(board);
        });

        assertThrows(MoveException.class, () -> {
            Move b5 = notation.parse("b5", BLACK, board);
            assertThat(b5.motivation(), is(MoveMotivation.PEACE ));
            assertThat(b5.type(), is(MoveType.TWO_SQUARE_FORWARD_FILE_ONLY_FIRST_MOVE));
            assertThat(b5.origin(), is(of("b7")));
            blackPlayer.movePiece(b5);
            print(b5);
            view.doDraw(board);
        });

        Move bxa4 = notation.parse("bxa4", BLACK, board);
        assertThat(bxa4.motivation(), is(MoveMotivation.ATTACK ));
        assertThat(bxa4.type(), is(MoveType.ONE_SQUARE_FORWARD_DIAGONAL_ATTACK_ONLY));
        assertThat(bxa4.origin(), is(of("b5")));
        blackPlayer.movePiece(bxa4);
        print(bxa4);
        view.doDraw(board);

        Move c8 = notation.parse("c8", WHITE, board);
        assertThat(c8.motivation(), is(MoveMotivation.PEACE));
        assertThat(c8.type(), is(MoveType.PROMOTION));
        assertThat(c8.origin(), is(of("c7")));
        whitePlayer.movePiece(c8);
        print(c8);
        view.doDraw(board);
        assertThat(board.getPieceByCoordinate(of("c8")).getClass(), is(Queen.class));

        Move rxc8 = notation.parse("Rxc8", BLACK, board);
        assertThat(rxc8.motivation(), is(MoveMotivation.ATTACK ));
        assertThat(rxc8.type(), is(MoveType.RANK));
        assertThat(rxc8.origin(), is(of("a8")));
        blackPlayer.movePiece(rxc8);
        print(rxc8);
        view.doDraw(board);

        Move nbd2 = notation.parse("Nbd2", WHITE, board);
        assertThat(nbd2.motivation(), is(MoveMotivation.PEACE));
        assertThat(nbd2.type(), is(MoveType.SECOND_SQUARE_NEIGHBORS_OPPOSITE_COLOR));
        assertThat(nbd2.origin(), is(of("b1")));
        whitePlayer.movePiece(nbd2);
        print(nbd2);
        view.doDraw(board);
        assertThat(board.getPieceByCoordinate(of("d2")).getClass(), is(Knight.class));
        assertThat(board.getPieceByCoordinate(of("f3")).getClass(), is(Knight.class));

        Move nxf3 = notation.parse("Nxf3", BLACK, board);
        assertThat(nxf3.motivation(), is(MoveMotivation.ATTACK ));
        assertThat(nxf3.type(), is(MoveType.SECOND_SQUARE_NEIGHBORS_OPPOSITE_COLOR));
        assertThat(nxf3.origin(), is(of("d4")));
        blackPlayer.movePiece(nxf3);
        print(nxf3);
        view.doDraw(board);


        assertThrows(MoveException.class, () -> {
            Move h4 = notation.parse("h4", WHITE, board);
            assertThat(h4.origin(), is(of("h2")));
            whitePlayer.movePiece(h4);
            print(h4);
            view.doDraw(board);
        });

        assertThrows(MoveException.class, () -> {
            Move h4 = notation.parse("hxh4", WHITE, board);
            assertThat(nxf3.motivation(), is(MoveMotivation.ATTACK ));
            assertThat(nxf3.type(), is(MoveType.TWO_SQUARE_FORWARD_FILE_ONLY_FIRST_MOVE));
            assertThat(h4.origin(), is(of("h2")));
            whitePlayer.movePiece(h4);
            print(h4);
            view.doDraw(board);
        });

        assertThrows(MoveException.class, () -> {
            Move hxg3 = notation.parse("hxg3", WHITE, board);
            assertThat(hxg3.motivation(), is(MoveMotivation.ATTACK ));
            assertThat(hxg3.type(), is(MoveType.ONE_SQUARE_FORWARD_DIAGONAL_ATTACK_ONLY));
            assertThat(hxg3.origin(), is(of("h2")));
            whitePlayer.movePiece(hxg3);
            print(hxg3);
            view.doDraw(board);
        });

        assertThrows(MoveException.class, () -> {
            Move nxe4 = notation.parse("Nxe4", WHITE, board);
            assertThat(nxe4.motivation(), is(MoveMotivation.ATTACK ));
            assertThat(nxe4.type(), is(MoveType.SECOND_SQUARE_NEIGHBORS_OPPOSITE_COLOR));
            assertThat(nxe4.origin(), is(of("d2")));
            whitePlayer.movePiece(nxe4);
            print(nxe4);
            view.doDraw(board);
        });

        Move kh1 = notation.parse("Kh1", WHITE, board);
        assertThat(kh1.motivation(), is(MoveMotivation.PEACE));
        assertThat(kh1.type(), is(MoveType.ONE_SQUARE_RANK));
        assertThat(kh1.origin(), is(of("g1")));
        whitePlayer.movePiece(kh1);
        print(kh1);
        view.doDraw(board);

        assertThrows(MoveException.class, () -> {
            Move giveUp = notation.parse("0-1", BLACK, board);
            assertThat(giveUp.motivation(), is(MoveMotivation.PEACE));
            assertThat(giveUp.type(), is(MoveType.WHITE_GIVE_UP));
            assertThat(giveUp.origin(), is(nullValue()));
            assertThat(giveUp.isResult(), is(true));
        });

        Move giveUp = notation.parse("1-0", BLACK, board);
        assertThat(giveUp.motivation(), is(MoveMotivation.PEACE));
        assertThat(giveUp.type(), is(MoveType.BLACK_GIVE_UP));
        assertThat(giveUp.origin(), is(nullValue()));
        assertThat(giveUp.isResult(), is(true));

        Move draw = notation.parse("1/2-1/2", WHITE, board);
        assertThat(draw.motivation(), is(MoveMotivation.PEACE));
        assertThat(draw.type(), is(MoveType.DRAW));
        assertThat(draw.origin(), is(nullValue()));
        assertThat(draw.isResult(), is(true));
    }

    @Test
    void moveFrom() {
        assertThat(notation.tryToRetrieveOriginFromNotation("Qh4e1"), is(of(3, 7)));
        assertThat(notation.tryToRetrieveOriginFromNotation("Qh4xe1+"), is(of(3, 7)));
        assertThat(notation.tryToRetrieveOriginFromNotation("Nfxd6"), is(nullValue()));
        assertThat(notation.tryToRetrieveOriginFromNotation("Rfe8"), is(nullValue()));
        assertThat(notation.tryToRetrieveOriginFromNotation("Nef7+"), is(nullValue()));
    }

    @Test
    void moveTo() {
        assertThat(notation.retrieveDestination("e4"), is(of(3, 4)));
        assertThat(notation.retrieveDestination("Nf3"), is(of(2, 5)));
        assertThat(notation.retrieveDestination("cxd4"), is(of(3, 3)));
        assertThat(notation.retrieveDestination("Nxd4"), is(of(3, 3)));
        assertThat(notation.retrieveDestination("Bg2"), is(of(1, 6)));
        assertThat(notation.retrieveDestination("Qe2"), is(of(1, 4)));
        assertThat(notation.retrieveDestination("0-0-0"), is(nullValue()));
        assertThat(notation.retrieveDestination("Rg8"), is(of(7, 6)));
        assertThat(notation.retrieveDestination("Kb1"), is(of(0, 1)));
        assertThat(notation.retrieveDestination("Rxc3"), is(of(2, 2)));
        assertThat(notation.retrieveDestination("Bxf4"), is(of(3, 5)));
        assertThat(notation.retrieveDestination("Be3+"), is(of(2, 4)));
        assertThat(notation.retrieveDestination("Nd4+"), is(of(3, 3)));
        assertThat(notation.retrieveDestination("bxc4+"), is(of(3, 2)));
        assertThat(notation.retrieveDestination("Kxc4"), is(of(3, 2)));
        assertThat(notation.retrieveDestination("0-1"), is(nullValue()));
    }

    @Test
    void findOutPieceType() {
        assertThat(notation.findOutPieceType("e4"), is(Pawn.class));
        assertThat(notation.findOutPieceType("Nf3"), is(Knight.class));
        assertThat(notation.findOutPieceType("cxd4"), is(Pawn.class));
        assertThat(notation.findOutPieceType("Nxd4"), is(Knight.class));
        assertThat(notation.findOutPieceType("Bg2"), is(Bishop.class));
        assertThat(notation.findOutPieceType("Qe2"), is(Queen.class));
        assertThat(notation.findOutPieceType("0-0-0"), is(King.class));
        assertThat(notation.findOutPieceType("Rg8"), is(Rook.class));
        assertThat(notation.findOutPieceType("Kb1"), is(King.class));
        assertThat(notation.findOutPieceType("Rxc3"), is(Rook.class));
        assertThat(notation.findOutPieceType("Bxf4"), is(Bishop.class));
        assertThat(notation.findOutPieceType("Be3+"), is(Bishop.class));
        assertThat(notation.findOutPieceType("Nd4+"), is(Knight.class));
        assertThat(notation.findOutPieceType("bxc4+"), is(Pawn.class));
        assertThat(notation.findOutPieceType("Kxc4"), is(King.class));
        assertThat(notation.findOutPieceType("0-1"), is(King.class));
    }

    @Test
    void isAttack() {
        assertThat(notation.isAttack("e4"), is(false));
        assertThat(notation.isAttack("Nf3"), is(false));
        assertThat(notation.isAttack("cxd4"), is(true));
        assertThat(notation.isAttack("Nxd4"), is(true));
        assertThat(notation.isAttack("Bg2"), is(false));
        assertThat(notation.isAttack("Qe2"), is(false));
        assertThat(notation.isAttack("0-0-0"), is(false));
        assertThat(notation.isAttack("Rg8"), is(false));
        assertThat(notation.isAttack("Kb1"), is(false));
        assertThat(notation.isAttack("Rxc3"), is(true));
        assertThat(notation.isAttack("Bxf4"), is(true));
        assertThat(notation.isAttack("Be3+"), is(false));
        assertThat(notation.isAttack("Nd4+"), is(false));
        assertThat(notation.isAttack("bxc4+"), is(true));
        assertThat(notation.isAttack("Kxc4"), is(true));
        assertThat(notation.isAttack("0-1"), is(false));
    }

    @Test
    void isCheck() {
        assertThat(notation.isCheck("e4"), is(false));
        assertThat(notation.isCheck("Nf3"), is(false));
        assertThat(notation.isCheck("cxd4"), is(false));
        assertThat(notation.isCheck("Nxd4"), is(false));
        assertThat(notation.isCheck("Bg2"), is(false));
        assertThat(notation.isCheck("Qe2"), is(false));
        assertThat(notation.isCheck("0-0-0"), is(false));
        assertThat(notation.isCheck("Rg8"), is(false));
        assertThat(notation.isCheck("Kb1"), is(false));
        assertThat(notation.isCheck("Rxc3"), is(false));
        assertThat(notation.isCheck("Bxf4"), is(false));
        assertThat(notation.isCheck("Be3+"), is(true));
        assertThat(notation.isCheck("Nd4+"), is(true));
        assertThat(notation.isCheck("bxc4+"), is(true));
        assertThat(notation.isCheck("Kxc4"), is(false));
        assertThat(notation.isCheck("0-1"), is(false));
    }

    @Test
    void allowedMoves() {
        Coordinate e2 = notation.retrieveDestination("e2");
        Coordinate e4 = notation.retrieveDestination("e4");
        assertThat(new Pawn(WHITE, e2).isValidMove(e4, MoveMotivation.PEACE), is(true));
        assertThat(new Pawn(WHITE, e2).isValidMove(e4, MoveMotivation.ATTACK), is(false));
    }

    private void print(Move move) {
        System.out.println(String.format("\n%s's move: %s", move.color(), move.notation()));
    }
}