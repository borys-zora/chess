package interview.chess.move;

import interview.chess.model.Color;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Borys Zora
 * @version 2020-03-06
 */
class CoordinateTest {

    @Test
    void coordinateOf() {
        var shift = Shift.of(2, 0).applyOnlyForColor(Color.WHITE).changeMotivation(MoveMotivation.PEACE);
        assertThat(Coordinate.of(Coordinate.of("e2"), shift), is(Coordinate.of("e4")));
    }
}