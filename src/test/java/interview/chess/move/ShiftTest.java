package interview.chess.move;

import org.junit.jupiter.api.Test;

import java.util.List;

import static interview.chess.move.Shift.of;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Borys Zora
 * @version 2020-03-06
 */
class ShiftTest {

    @Test
    void obstacleSteps() {
        var obstacles = of(3, 3).obstacleSteps();
        assertThat(obstacles.size(), is(2));
        assertThat(obstacles, is(List.of(of(1, 1), of(2, 2))));

        obstacles = of(-4, 4).obstacleSteps();
        assertThat(obstacles.size(), is(3));
        assertThat(obstacles, is(List.of(of(-1, 1), of(-2, 2), of(-3, 3))));

        obstacles = of(-2, -2).obstacleSteps();
        assertThat(obstacles.size(), is(1));
        assertThat(obstacles, is(List.of(of(-1, -1))));

        obstacles = of(2, -2).obstacleSteps();
        assertThat(obstacles.size(), is(1));
        assertThat(obstacles, is(List.of(of(1, -1))));

        obstacles = of(0, -2).obstacleSteps();
        assertThat(obstacles.size(), is(1));
        assertThat(obstacles, is(List.of(of(0, -1))));

        obstacles = of(0, 3).obstacleSteps();
        assertThat(obstacles.size(), is(2));
        assertThat(obstacles, is(List.of(of(0, 1), of(0, 2))));

        obstacles = of(-3, 0).obstacleSteps();
        assertThat(obstacles.size(), is(2));
        assertThat(obstacles, is(List.of(of(-1, 0), of(-2, 0))));
    }

}