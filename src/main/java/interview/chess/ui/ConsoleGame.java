package interview.chess.ui;

import interview.chess.game.Drawable;
import interview.chess.game.PieceMappingAware;
import interview.chess.game.Game;

import java.util.Scanner;

/**
 * @author Borys Zora
 * @version 2020-03-05
 */
public class ConsoleGame extends Game {

    private Drawable drawable = new BoardView();
    private Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        new ConsoleGame().start();
    }

    @Override
    protected String nextUserInput() {
        return scanner.next();
    }

    @Override
    public void doDraw(PieceMappingAware pieceMappingAware) {
        drawable.doDraw(pieceMappingAware);
    }
}
