package interview.chess.ui;

import interview.chess.game.PieceMappingAware;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

/**
 * @author Borys Zora
 * @version 2020-03-05
 */
class AlgebraicNotationGameReader extends ConsoleGame {

    private int totalMoves = 0;
    private Queue<String> gameRecording = new LinkedList<>();
    private String currentMove = "";

    AlgebraicNotationGameReader(String gameRecording) {
        parse(gameRecording);
    }

    public static void main(String[] args) {
        System.out.println(String.join(" ", args));
        if (args.length == 0) {
            System.out.println("mandatory parameter is required: file location");
        }
        new AlgebraicNotationGameReader(args[0]).start();
    }

    private void parse(String gameRecordingFile) {
        String content = readFile(gameRecordingFile);
        Arrays.stream(content.split("\\s"))
                .map(this::removeMoveNumber)
                .filter(Objects::nonNull)
                .forEach(w -> gameRecording.add(w));
    }

    private String removeMoveNumber(String move) {
        if (move == null || move.length() < 2) {
            return null;
        }
        if (move.contains(".")) {
            return move.split("\\.")[1];
        }
        return move;
    }

    @Override
    protected String nextUserInput() {
        totalMoves++;
        currentMove = gameRecording.poll();
        return currentMove;
    }

    @Override
    public void doDraw(PieceMappingAware pieceMappingAware) {
        System.out.println(currentMove);
        super.doDraw(pieceMappingAware);
    }

    int totalMoves() {
        return totalMoves;
    }

    private String readFile(String file) {
        try {
            return new String(getClass().getClassLoader().getResourceAsStream(file).readAllBytes());
        } catch (IOException e) {
            throw new RuntimeException("Could not read file: " + file);
        }
    }
}
