package interview.chess.ui;

import interview.chess.game.Drawable;
import interview.chess.game.PieceMappingAware;
import interview.chess.move.Coordinate;
import interview.chess.model.*;

import java.util.Map;

import static java.lang.String.format;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public class BoardView implements Drawable {

    private static final String ANSI_BLACK_BACKGROUND = "\u001B[47m";
    private static final String ANSI_WHITE_BACKGROUND = "\u001B[48m";
    private static final String ANSI_BOARD_LINE_BACKGROUND = "\033[43m";
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String WIDTH = "       ";
    private static final String HALF_WITH = "   ";
    private static final String WHITE_LINE = ANSI_WHITE_BACKGROUND + WIDTH + ANSI_RESET;
    private static final String WHITE_VALUE_LINE = ANSI_WHITE_BACKGROUND + HALF_WITH + "%s" + HALF_WITH + ANSI_RESET;
    private static final String BLACK_LINE = ANSI_BLACK_BACKGROUND + WIDTH + ANSI_RESET;
    private static final String BLACK_VALUE_LINE = ANSI_BLACK_BACKGROUND + HALF_WITH + "%s" + HALF_WITH + ANSI_RESET;

    @Override
    public void doDraw(PieceMappingAware pieceMappingAware) {
        Map<Coordinate, Piece> pieceMap = pieceMappingAware.pieceMap();
        print(caption());
        for(int row = 7; row >= 0; row--) {
            Square[] squares = new Square[8];
            for (int column = 7; column >= 0; column--) {
                Coordinate coordinate = Coordinate.of(row, column);
                Piece piece = pieceMap.get(coordinate);
                String preview = piece != null ? piece.preview() : " ";
                squares[column] = createSquare(row, column, preview);
            }
            print(squares, row + 1);
        }
        print(caption());
    }

    private String caption() {
        String delimiter = WIDTH.substring(1);
        String[] caption = {"a", "b", "c", "d", "e", "f", "g", "h"};
        return ANSI_BOARD_LINE_BACKGROUND + delimiter + String.join(delimiter, caption)
                + delimiter + ANSI_RESET;
    }

    private void print(Square[] row, int rowNumber) {
        String result = new Row(row, rowNumber).toString();
        print(result);
    }

    private void print(String row) {
        System.out.println(row);
    }

    private Square createSquare(int row, int column, String value) {
        return (row + column) % 2 == 0 ? createBlackSquare(value) : createWhiteSquare(value);
    }

    private Square createWhiteSquare(String value) {
        return new Square(WHITE_LINE, WHITE_VALUE_LINE, value);
    }

    private Square createBlackSquare(String value) {
        return new Square(BLACK_LINE, BLACK_VALUE_LINE, value);
    }

    private class Row {
        String topAndBottomLine = "";
        String valueLine = "";
        int rowNumber;

        Row(Square[] row, int rowNumber) {
            this.rowNumber = rowNumber;
            for(Square square: row) {
                topAndBottomLine += square.topAndBottomLine;
                valueLine += square.valueLine;
            }
        }

        String valueLine() {
            return  ANSI_BOARD_LINE_BACKGROUND + " " + rowNumber + " " + ANSI_RESET + valueLine +
                    ANSI_BOARD_LINE_BACKGROUND + " " + rowNumber + " " + ANSI_RESET;
        }

        String topAndBottomLine() {
            return  ANSI_BOARD_LINE_BACKGROUND + "   " + ANSI_RESET + topAndBottomLine +
                    ANSI_BOARD_LINE_BACKGROUND + "   " + ANSI_RESET;
        }

        @Override
        public String toString() {
            return topAndBottomLine() + "\n" + valueLine() + "\n" + topAndBottomLine();
        }
    }

    private class Square {
        String topAndBottomLine = "";
        String valueLine = "";

        Square(String topAndBottomLine, String valuePattern, String value) {
            this.topAndBottomLine = topAndBottomLine;
            this.valueLine = format(valuePattern, value);
        }
    }
}
