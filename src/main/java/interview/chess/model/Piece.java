package interview.chess.model;

import interview.chess.move.Coordinate;
import interview.chess.move.MoveMotivation;
import interview.chess.move.MoveType;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import static interview.chess.move.Coordinate.of;
import static interview.chess.move.MoveMotivation.ATTACK;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public abstract class Piece {

    private Color color;
    private Coordinate coordinate;
    protected Stack<Coordinate> tracing = new Stack<>();

    public Piece(Color color, Coordinate coordinate) {
        this.color = color;
        this.coordinate = coordinate;
    }

    public void move(Coordinate to) {
        tracing.push(this.coordinate);
        this.coordinate = to;
    }

    public void rollback() {
        coordinate = tracing.pop();
    }

    public Color color() {
        return color;
    }

    public Coordinate coordinate() {
        return coordinate;
    }

    public abstract List<MoveType> allowedMoves();

    public abstract String preview();

    public boolean isValidMove(Coordinate to, MoveMotivation motivation) {
        return possibleCoordinates(motivation).contains(to);
    }

    public Set<Coordinate> possibleCoordinates(MoveMotivation motivation) {
        return allowedMoves().stream()
                .flatMap(t -> t.shifts().stream())
                .filter(s -> s.isApplicable(motivation))
                .map(s -> of(coordinate, s))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    public Set<Coordinate> possibleAttacks() {
        return allowedMoves().stream()
                .flatMap(t -> t.shifts().stream())
                .filter(s -> s.isApplicable(ATTACK))
                .map(s -> of(coordinate, s))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    public void die() {
        coordinate = null;
    }

    public int moveCount() {
        return tracing.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Piece)) return false;

        Piece piece = (Piece) o;

        if (color != piece.color) return false;
        return coordinate != null ? coordinate.equals(piece.coordinate) : piece.coordinate == null;
    }

    @Override
    public int hashCode() {
        int result = color != null ? color.hashCode() : 0;
        result = 31 * result + (coordinate != null ? coordinate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", color.name(), getClass().getSimpleName(), coordinate().asString());
    }
}
