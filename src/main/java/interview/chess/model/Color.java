package interview.chess.model;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public enum Color {

    WHITE,
    BLACK;

    public Color opponent() {
        return (this == WHITE) ? BLACK : WHITE;
    }
}
