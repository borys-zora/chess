package interview.chess.model;

import interview.chess.move.Coordinate;
import interview.chess.move.MoveType;

import java.util.List;

import static interview.chess.move.MoveType.FILE;
import static interview.chess.move.MoveType.RANK;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public class Rook extends Piece {

    public Rook(Color color, Coordinate coordinate) {
        super(color, coordinate);
    }

    @Override
    public List<MoveType> allowedMoves() {
        return List.of(FILE, RANK);
    }

    @Override
    public String preview() {
        return color() == Color.WHITE ? "\u2656" : "\u265C";
    }
}
