package interview.chess.model;

import interview.chess.move.Coordinate;
import interview.chess.move.MoveType;

import java.util.List;

import static interview.chess.move.MoveType.*;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public class Queen extends Piece {

    public Queen(Color color, Coordinate coordinate) {
        super(color, coordinate);
    }

    @Override
    public List<MoveType> allowedMoves() {
        return List.of(DIAGONAL, FILE, RANK);
    }

    @Override
    public String preview() {
        return color() == Color.WHITE ? "\u2655" : "\u265B";
    }
}
