package interview.chess.model;

import interview.chess.move.Coordinate;
import interview.chess.move.MoveType;

import java.util.List;
import static interview.chess.move.MoveType.DIAGONAL;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public class Bishop extends Piece {

    public Bishop(Color color, Coordinate coordinate) {
        super(color, coordinate);
    }

    @Override
    public List<MoveType> allowedMoves() {
        return List.of(DIAGONAL);
    }

    @Override
    public String preview() {
        return color() == Color.WHITE ? "\u2657" : "\u265D";
    }
}
