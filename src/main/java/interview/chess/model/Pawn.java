package interview.chess.model;

import interview.chess.move.Coordinate;
import interview.chess.move.MoveType;

import java.util.List;

import static interview.chess.move.MoveType.*;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public class Pawn extends Piece {

    public Pawn(Color color, Coordinate coordinate) {
        super(color, coordinate);
    }

    public Piece promote(Class clazz) {
        Piece piece = null;
        if (clazz == Rook.class) {
            piece = new Rook(color(), coordinate());
        } else if (clazz == Bishop.class) {
            piece = new Bishop(color(), coordinate());
        } else if (clazz == Knight.class) {
            piece = new Knight(color(), coordinate());
        } else {
            piece = new Queen(color(), coordinate());
        }
        piece.tracing = tracing;
        return piece;
    }

    @Override
    public List<MoveType> allowedMoves() {
        return List.of(
                ONE_SQUARE_FORWARD_FILE,
                TWO_SQUARE_FORWARD_FILE_ONLY_FIRST_MOVE,
                ONE_SQUARE_FORWARD_DIAGONAL_ATTACK_ONLY,
                EN_PASSANT
        );
    }

    @Override
    public String preview() {
        return color() == Color.WHITE ? "\u2659" : "\u265F";
    }
}
