package interview.chess.model;

import interview.chess.move.Coordinate;
import interview.chess.move.MoveType;

import java.util.List;
import static interview.chess.move.MoveType.*;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public class King extends Piece {

    public King(Color color, Coordinate coordinate) {
        super(color, coordinate);
    }

    @Override
    public List<MoveType> allowedMoves() {
        return List.of(
                ONE_SQUARE_FILE,
                ONE_SQUARE_RANK,
                ONE_SQUARE_DIAGONAL,
                SQUARE_UNDER_ATTACK_FORBIDDEN
        );
    }

    @Override
    public String preview() {
        return color() == Color.WHITE ? "\u2654" : "\u265A";
    }
}
