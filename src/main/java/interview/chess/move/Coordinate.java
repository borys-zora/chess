package interview.chess.move;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public final class Coordinate {

    private static final int MAX = 7;
    private static final int MIN = 0;

    private final int row;
    private final int column;

    private Coordinate(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public static Coordinate of(int row, int column) {
        if(valid(row) && valid(column)) {
            return new Coordinate(row, column);
        }
        return null;
    }

    public static Coordinate of(Coordinate from, Shift shift) {
        int row = from.row + shift.row();
        int column = from.column + shift.column();
        return of(row, column);
    }

    public static Coordinate of(String coordinates) {
        if (coordinates.length() != 2) return null;
        char[] chars = coordinates.toCharArray();
        return toCoordinate(chars[0], chars[1]);
    }

    public static Coordinate toCoordinate(char columnChar, char rowChar) {
        int column = column(columnChar);
        int row = row(rowChar);
        return of(row, column);
    }

    public static int column(char columnChar) {
        return columnChar - 'a';
    }

    private static int row(char rowChar) {
        return rowChar - '1';
    }

    public int row() {
        return row;
    }

    public int column() {
        return column;
    }

    public String asString() {
        char columnChar = (char) (column + 'a');
        char rowChar = (char) (row + '1');
        char[] chars = {columnChar, rowChar};
        return new String(chars);
    }

    private static boolean valid(int index) {
        return index >= MIN && index <= MAX;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coordinate)) return false;

        Coordinate that = (Coordinate) o;

        if (row != that.row) return false;
        return column == that.column;
    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + column;
        return result;
    }

    @Override
    public String toString() {
        return asString() + "(  row: " + row + "  column: " + column + "  )";
    }
}
