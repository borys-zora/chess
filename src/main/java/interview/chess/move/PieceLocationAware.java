package interview.chess.move;

import interview.chess.model.Color;

import java.util.List;

/**
 * @author Borys Zora
 * @version 2020-03-07
 */
public interface PieceLocationAware {

    List<MoveType> allowedMoves(Coordinate origin);

    List<Coordinate> possibleOrigins(Class pieceType, Color color, Coordinate destination, MoveMotivation motivation);
}
