package interview.chess.move.validation;

import interview.chess.model.Color;
import interview.chess.model.Piece;

/**
 * @author Borys Zora
 * @version 2020-03-09
 */
public interface KingProtectedResolver extends ValidationResolver {

    Piece findKing(Color color);

}
