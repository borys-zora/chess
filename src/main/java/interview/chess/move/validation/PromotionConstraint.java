package interview.chess.move.validation;

import interview.chess.model.Pawn;
import interview.chess.model.Piece;
import interview.chess.model.Queen;
import interview.chess.move.Coordinate;

/**
 * @author Borys Zora
 * @version 2020-03-09
 */
public class PromotionConstraint implements ValidationConstraint<PromotionResolver> {

    private Coordinate destination;
    private boolean resolved = false;

    public PromotionConstraint(Coordinate destination) {
        this.destination = destination;
    }

    @Override
    public void resolve(PromotionResolver resolver) {
        Piece piece = resolver.getPieceByCoordinate(destination);
        if (piece.getClass() != Pawn.class) return;
        Piece promoted = ((Pawn) piece).promote(Queen.class);
        resolver.swap(piece, promoted);
        resolved = promoted.equals(resolver.getPieceByCoordinate(destination));
    }

    @Override
    public boolean isResolved() {
        return resolved;
    }

    @Override
    public String errorMessage() {
        return "Pawn promotion failed, destination: " + destination;
    }
}
