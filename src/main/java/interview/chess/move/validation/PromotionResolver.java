package interview.chess.move.validation;

import interview.chess.model.Piece;

/**
 * @author Borys Zora
 * @version 2020-03-09
 */
public interface PromotionResolver extends ValidationResolver, LocationResolver {

    void swap(Piece piece, Piece promoted);

}
