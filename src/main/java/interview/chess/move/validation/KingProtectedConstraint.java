package interview.chess.move.validation;

import interview.chess.model.Color;
import interview.chess.model.Piece;

/**
 * @author Borys Zora
 * @version 2020-03-09
 */
public class KingProtectedConstraint implements ValidationConstraint<KingProtectedResolver> {

    private Color color;
    private ValidationConstraint constraint;

    KingProtectedConstraint(Color color) {
        this.color = color;
    }

    @Override
    public void resolve(KingProtectedResolver resolver) {
        Piece piece = resolver.findKing(color);
        constraint = new SquareIsNotUnderAttackConstraint(piece.coordinate(), color.opponent());
        constraint.resolve(resolver);
    }

    @Override
    public boolean isResolved() {
        return constraint.isResolved();
    }

    @Override
    public String errorMessage() {
        return constraint.errorMessage();
    }
}
