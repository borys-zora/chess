package interview.chess.move.validation;

import interview.chess.model.Color;
import interview.chess.move.Coordinate;

/**
 * @author Borys Zora
 * @version 2020-03-09
 */
public interface SquareIsUnderAttackResolver extends ValidationResolver {

    boolean squareIsNotAttackedByOpponent(Coordinate coordinate, Color opponentsColor);

}
