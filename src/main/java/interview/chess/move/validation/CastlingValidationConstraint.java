package interview.chess.move.validation;

import interview.chess.model.Color;
import interview.chess.move.Coordinate;
import interview.chess.move.MoveType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;

/**
 * @author Borys Zora
 * @version 2020-03-07
 */
public class CastlingValidationConstraint implements ValidationConstraint<CastlingValidationResolver> {

    private final Coordinate kingCoordinate;
    private final Coordinate rookCoordinate;
    private final MoveType type;
    private final Color kingsColor;
    private final List<ValidationConstraint> constraints = new ArrayList<>();
    private boolean resolved = false;

    CastlingValidationConstraint(Coordinate king, Coordinate rook, MoveType type, Color color) {
        this.kingCoordinate = king;
        this.rookCoordinate = rook;
        this.type = type;
        this.kingsColor = color;
        addConstraints();
    }

    private void addConstraints() {
        addSquareIsNotUnderAttackConstraints();
        addFirstMoveConstraints();
        addEmptySquareValidationConstraints();
    }

    private void addSquareIsNotUnderAttackConstraints() {
        constraints.addAll(type.attackSensitive().stream()
                .map(s -> Coordinate.of(kingCoordinate, s))
                .map(c -> new SquareIsNotUnderAttackConstraint(c, kingsColor.opponent()))
                .collect(Collectors.toList()));
    }

    private void addFirstMoveConstraints() {
        constraints.add(new FirstMoveConstraint(kingCoordinate));
        constraints.add(new FirstMoveConstraint(rookCoordinate));
    }

    private void addEmptySquareValidationConstraints() {
        constraints.addAll(type.emptyShifts().stream()
                .map(s -> Coordinate.of(kingCoordinate, s))
                .map(EmptySquareValidationConstraint::new)
                .collect(Collectors.toList()));
    }

    @Override
    public void resolve(CastlingValidationResolver resolver) {
        constraints.forEach(c -> c.resolve(resolver));
        resolved = constraints.stream().allMatch(ValidationConstraint::isResolved);
    }

    @Override
    public boolean isResolved() {
        return resolved;
    }

    @Override
    public String errorMessage() {
        return resolved ? "" : "castling validation error: " + constraints.stream()
                .filter(not(ValidationConstraint::isResolved))
                .map(ValidationConstraint::errorMessage)
                .collect(Collectors.joining("\n"));
    }
}
