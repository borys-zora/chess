package interview.chess.move.validation;

/**
 * @author Borys Zora
 * @version 2020-03-05
 */
public interface ValidationConstraint<T extends ValidationResolver> {

    void resolve(T resolver);

    boolean isResolved();

    String errorMessage();
}
