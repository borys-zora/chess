package interview.chess.move.validation;

import interview.chess.model.Color;
import interview.chess.model.Pawn;
import interview.chess.move.Coordinate;
import interview.chess.move.Move;
import interview.chess.move.exception.MoveException;

import static interview.chess.move.Coordinate.of;
import static interview.chess.move.MoveType.*;
import static interview.chess.move.MovesLog.*;

/**
 * @author Borys Zora
 * @version 2020-03-08
 */
public class SpecialMoveRules extends MoveValidationRules {

    public void setupPromotion(Move move) throws MoveException {
        move.setType(PROMOTION);
        addMotivationRule(move);
        addKingProtectedConstraint(move);
        move.addAfterMoveConstraint(new PromotionConstraint(move.destination()));
    }

    public void setupCastlingKingSide(Move move) throws MoveException {
        move.setType(CASTLING_KING_SIDE);
        setupCastling(move);
    }

    public void setupCastingQueenSide(Move move) throws MoveException {
        move.setType(CASTLING_QUEEN_SIDE);
        setupCastling(move);
    }

    private void setupCastling(Move move) {
        var origin = (move.color() == Color.WHITE) ? of("e1") : of("e8");
        move.setOrigin(origin);
        move.setDestination(of(origin, move.type().primaryItemShift()));
        setupRookMove(move);
        var rookOrigin = move.secondMove().origin();
        move.addConstraint(new CastlingValidationConstraint(origin, rookOrigin, move.type(), move.color()));
    }

    private void setupRookMove(Move move) {
        var rookOrigin = of(move.origin(), move.type().secondItemLocation());
        var rootDestination = of(rookOrigin, move.type().secondItemShift());
        move.setupSecondMove(rookOrigin, rootDestination);
    }

    public boolean isEnPassant(Move move, Coordinate destination, Class pieceType) {
        if (pieceType != Pawn.class) return false;
        var lastMovingPiece = lastMovingPiece();
        if (lastMovingPiece == null) return false;
        return lastMovingPiece.getClass() == Pawn.class
                && lastMovingType() == TWO_SQUARE_FORWARD_FILE_ONLY_FIRST_MOVE
                && move.isAttack()
                && ((destination.row() == 5 && move.color() == Color.WHITE)
                   || (destination.row() == 2 && move.color() == Color.BLACK))
                && lastMovingPiece.coordinate().column() == destination.column();
    }

    public void setupEnPassant(Move move) throws MoveException {
        move.setType(EN_PASSANT);
        var lastMovingPiece = lastMovingPiece();
        move.specifyAttackLocation(lastMovingPiece.coordinate());
    }

    public void whiteGiveUp(Move move) throws MoveException {
        move.setType(WHITE_GIVE_UP);
    }

    public void blackGiveUp(Move move) throws MoveException {
        move.setType(BLACK_GIVE_UP);
    }

    public void draw(Move move) throws MoveException {
        move.setType(DRAW);
    }
}
