package interview.chess.move.validation;

import interview.chess.move.Coordinate;

/**
 * @author Borys Zora
 * @version 2020-03-09
 */
public class NonEmptySquareValidationConstraint implements ValidationConstraint<LocationResolver>  {

    private final Coordinate coordinate;
    private boolean resolved = false;

    public NonEmptySquareValidationConstraint(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    @Override
    public void resolve(LocationResolver resolver) {
        resolved = resolver.getPieceByCoordinate(coordinate) != null;
    }

    @Override
    public boolean isResolved() {
        return resolved;
    }

    @Override
    public String errorMessage() {
        return resolved ? "" : coordinate.asString() + " must be non empty to complete the move";
    }
}
