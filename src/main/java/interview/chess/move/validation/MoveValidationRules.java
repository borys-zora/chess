package interview.chess.move.validation;

import interview.chess.move.Coordinate;
import interview.chess.move.Move;
import interview.chess.move.MoveMotivation;

import java.util.stream.Collectors;

/**
 * @author Borys Zora
 * @version 2020-03-05
 */
public class MoveValidationRules {

    public void addConstraints(Move move) {
        addEmptySquaresConstraint(move);
        addMotivationRule(move);
        addKingProtectedConstraint(move);
    }

    protected void addMotivationRule(Move move) {
        if (move.motivation() == MoveMotivation.ATTACK) {
            move.addConstraint(new NonEmptySquareValidationConstraint(move.destination()));
        } else {
            move.addConstraint(new EmptySquareValidationConstraint(move.destination()));
        }
    }

    protected void addEmptySquaresConstraint(Move move) {
        move.shift().obstacleSteps().stream()
                .map(s -> Coordinate.of(move.origin(), s))
                .map(EmptySquareValidationConstraint::new)
                .collect(Collectors.toList())
                .forEach(move::addConstraint);
    }

    protected void addKingProtectedConstraint(Move move) {
        move.addAfterMoveConstraint(new KingProtectedConstraint(move.color()));
    }

}
