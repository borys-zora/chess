package interview.chess.move.validation;

import interview.chess.model.Piece;
import interview.chess.move.Coordinate;

/**
 * @author Borys Zora
 * @version 2020-03-09
 */
public interface LocationResolver extends ValidationResolver {

    Piece getPieceByCoordinate(Coordinate coordinate);

}
