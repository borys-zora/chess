package interview.chess.move.validation;

import interview.chess.model.Color;
import interview.chess.move.Coordinate;

import static java.lang.String.format;

/**
 * @author Borys Zora
 * @version 2020-03-09
 */
public class SquareIsNotUnderAttackConstraint implements ValidationConstraint<SquareIsUnderAttackResolver> {

    private Coordinate coordinate;
    private Color opponentsColor;
    private boolean resolved;

    SquareIsNotUnderAttackConstraint(Coordinate coordinate, Color opponentsColor) {
        this.coordinate = coordinate;
        this.opponentsColor = opponentsColor;
    }

    @Override
    public void resolve(SquareIsUnderAttackResolver resolver) {
        resolved = resolver.squareIsNotAttackedByOpponent(coordinate, opponentsColor);
    }

    @Override
    public boolean isResolved() {
        return resolved;
    }

    @Override
    public String errorMessage() {
        return resolved ? "" : format("square %s is under attack", coordinate.toString());
    }

}
