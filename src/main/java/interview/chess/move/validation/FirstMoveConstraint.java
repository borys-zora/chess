package interview.chess.move.validation;

import interview.chess.model.Piece;
import interview.chess.move.Coordinate;

/**
 * @author Borys Zora
 * @version 2020-03-09
 */
public class FirstMoveConstraint implements ValidationConstraint<LocationResolver> {

    private Coordinate origin;
    private boolean resolved = false;

    public FirstMoveConstraint(Coordinate origin) {
        this.origin = origin;
    }

    @Override
    public void resolve(LocationResolver resolver) {
        Piece piece = resolver.getPieceByCoordinate(origin);
        resolved = (piece.moveCount() == 0);
    }

    @Override
    public boolean isResolved() {
        return resolved;
    }

    @Override
    public String errorMessage() {
        return origin.toString() + "has been already moved";
    }
}
