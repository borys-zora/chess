package interview.chess.move.exception;

/**
 * @author Borys Zora
 * @version 2020-03-06
 */
public class CouldNotIdentifyMovingPieceException extends MoveException {

    public CouldNotIdentifyMovingPieceException(String message) {
        super(message);
    }

}
