package interview.chess.move.exception;

/**
 * @author Borys Zora
 * @version 2020-03-06
 */
public class MoveException extends Exception {

    public MoveException(String message) {
        super(message);
    }
}
