package interview.chess.move.exception;

/**
 * @author Borys Zora
 * @version 2020-03-06
 */
public class MoveValidationException extends MoveException {

    public MoveValidationException(String message) {
        super(message);
    }

}
