package interview.chess.move;

import interview.chess.model.Color;
import interview.chess.move.exception.MoveException;
import interview.chess.move.validation.ValidationConstraint;
import interview.chess.move.validation.ValidationResolver;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static interview.chess.move.MoveMotivation.ATTACK;
import static interview.chess.move.MoveMotivation.PEACE;
import static java.util.function.Predicate.not;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public class Move {

    private String notation;
    private Color color;
    private MoveType type;
    private Shift shift;
    private List<ValidationConstraint> constraints = new ArrayList<>();
    private List<ValidationConstraint> afterMoveConstraints = new ArrayList<>();
    private Coordinate origin;
    private Coordinate destination;
    private Coordinate attackLocation;
    private MoveMotivation motivation;
    private boolean check = false;
    private boolean special = false;
    private boolean result = false;
    private String resultMessage;
    private Move secondMove;

    public Move(String notation, Color color) {
        this.notation = notation;
        this.color = color;
    }

    private Move(Coordinate origin, Coordinate destination) {
        this.origin = origin;
        this.destination = destination;
    }

    public Coordinate destination() {
        return destination;
    }

    public Color color() {
        return color;
    }

    public Shift shift() {
        return shift;
    }

    public String notation() {
        return notation;
    }

    Move setAttack(boolean attack) {
        motivation = attack ? ATTACK : PEACE;
        return this;
    }

    public boolean isAttack() {
        return motivation == ATTACK;
    }

    public MoveMotivation motivation() {
        return motivation;
    }

    public void setType(MoveType type) throws MoveException {
        validateIfResult(type);
        this.type = type;
    }

    private void validateIfResult(MoveType type) throws MoveException {
        if (type == MoveType.WHITE_GIVE_UP) {
            if (color != Color.WHITE) {
                throw new MoveException("Only WHITE could give up on WHITE turn");
            }
            resultMessage = "WHITE gave up\n";
        }
        if (type == MoveType.BLACK_GIVE_UP) {
            if (color != Color.BLACK) {
                throw new MoveException("Only BLACK could give up on BLACK turn");
            }
            resultMessage = "BLACK gave up\n";
        }
        if (type == MoveType.DRAW) {
            resultMessage = "draw negotiated\n";
        }
        result = (resultMessage != null);
    }

    public MoveType type() {
        return type;
    }

    public Move setDestination(Coordinate destination) {
        this.destination = destination;
        return this;
    }

    Move setCheck(boolean check) {
        this.check = check;
        return this;
    }

    public Coordinate origin() {
        return origin;
    }

    public Move setOrigin(Coordinate origin) {
        this.origin = origin;
        return this;
    }

    public void addConstraint(ValidationConstraint constraint) {
        constraints.add(constraint);
    }

    public void addAfterMoveConstraint(ValidationConstraint constraint) {
        afterMoveConstraints.add(constraint);
    }

    public void resolveConstraints(ValidationResolver resolver) {
        constraints.forEach(c -> c.resolve(resolver));
    }

    public void resolveAfterMoveConstraints(ValidationResolver resolver) {
        afterMoveConstraints.forEach(c -> c.resolve(resolver));
    }

    public boolean isValid() {
        return constraints.stream().allMatch(ValidationConstraint::isResolved);
    }

    public boolean isValidAfterMove() {
        return afterMoveConstraints.stream().allMatch(ValidationConstraint::isResolved);
    }

    public List<String> constrainsMessages() {
        return constraints.stream()
                .filter(not(ValidationConstraint::isResolved))
                .map(ValidationConstraint::errorMessage)
                .collect(Collectors.toList());
    }

    public List<String> afterMoveConstrainsMessages() {
        return afterMoveConstraints.stream()
                .filter(not(ValidationConstraint::isResolved))
                .map(ValidationConstraint::errorMessage)
                .collect(Collectors.toList());
    }

    void makeSpecial() {
        this.special = true;
    }

    public boolean isSpecial() {
        return special;
    }

    public Coordinate attackLocation() {
        return (attackLocation != null) ? attackLocation : destination;
    }

    public void specifyAttackLocation(Coordinate coordinate) {
        attackLocation = coordinate;
    }

    public boolean isDoubleMove() {
        return secondMove != null;
    }

    public Move secondMove() {
        return secondMove;
    }

    public void setupSecondMove(Coordinate origin, Coordinate destination) {
        secondMove = new Move(origin, destination);
        secondMove.type = type;
        secondMove.notation = notation;
    }

    void setupTypeAndShift(List<MoveType> allowedMoves) {
        for(MoveType type: allowedMoves) {
            var shift = targetShift(type);
            if (shift != null) {
                this.type = type;
                this.shift = shift;
                return;
            }
        }
    }

    private Shift targetShift(MoveType type) {
        return type.shifts().stream()
                .filter(s -> s.isApplicable(color()))
                .filter(s -> s.isApplicable(motivation()))
                .filter(s -> shiftMatchesDestination(s))
                .findFirst().orElse(null);
    }

    private boolean shiftMatchesDestination(Shift shift) {
        Coordinate coordinate = Coordinate.of(origin(), shift);
        return (coordinate != null) && coordinate.equals(destination());
    }

    public boolean isResult() {
        return result;
    }

    public String resultMessage() {
        return resultMessage;
    }
}
