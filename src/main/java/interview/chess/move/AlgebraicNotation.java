package interview.chess.move;

import interview.chess.model.*;
import interview.chess.move.exception.CouldNotIdentifyMovingPieceException;
import interview.chess.move.exception.MoveException;
import interview.chess.move.validation.SpecialMoveRules;

import static interview.chess.move.Coordinate.of;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public class AlgebraicNotation {

    private static final String ATTACK_SYMBOL = "x";
    private static final String CHECK_SYMBOL = "+";

    private SpecialMoveRules rules = new SpecialMoveRules();

    public Move parse(String notation, Color color, PieceLocationAware locationAware) throws MoveException {
        Move move = new Move(notation, color)
                .setAttack(isAttack(notation))
                .setCheck(isCheck(notation));
        if (isSpecial(move)) {
            populateSpecial(move, locationAware);
        } else {
            populateRegular(move, locationAware);
        }
        return move;
    }

    private void populateRegular(Move move, PieceLocationAware locationAware) throws MoveException {
        move.setDestination(retrieveDestination(move.notation()));
        populateOrigin(move, locationAware);
        var allowedMoves = locationAware.allowedMoves(move.origin());
        move.setupTypeAndShift(allowedMoves);
        rules.addConstraints(move);
    }

    private void populateSpecial(Move move, PieceLocationAware locationAware) throws MoveException {
        move.makeSpecial();
        if (isResult(move.notation())) {
            populateResult(move);
        } else if (isCastling(move.notation())) {
            populateCastling(move);
        } else if (isPromotion(move.notation())) {
            populatePromotion(move, locationAware);
        } else {
            populateEnPassant(move, locationAware);
        }
    }

    private void populateResult(Move move) throws MoveException {
        String notation = move.notation();
        if (isWhiteGiveUp(notation)) {
            rules.whiteGiveUp(move);
        } else if (isBlackGiveUp(notation)) {
            rules.blackGiveUp(move);
        } else {
            rules.draw(move);
        }
    }

    private void populatePromotion(Move move, PieceLocationAware locationAware) throws MoveException {
        move.setDestination(retrieveDestination(move.notation()));
        populateOrigin(move, locationAware);
        rules.setupPromotion(move);
    }

    private void populateCastling(Move move) throws MoveException {
        if (isCastlingKingSide(move.notation())) {
            rules.setupCastlingKingSide(move);
        } else {
            rules.setupCastingQueenSide(move);
        }
    }

    private void populateOrigin(Move move, PieceLocationAware locationAware) throws MoveException {
        Coordinate movedFrom = tryToRetrieveOriginFromNotation(move.notation());
        if(movedFrom != null) {
            move.setOrigin(movedFrom);
            return;
        }
        populateOriginUsingLocationContext(move, locationAware);
    }

    private void populateOriginUsingLocationContext(Move move, PieceLocationAware locationAware)
            throws CouldNotIdentifyMovingPieceException {
        Class pieceType = findOutPieceType(move.notation());
        var possibleOrigins = locationAware.possibleOrigins(pieceType, move.color(), move.destination(), move.motivation());
        if(possibleOrigins.isEmpty()) {
            throw new CouldNotIdentifyMovingPieceException("Could not identify move by command: " + move.notation());
        }
        if (possibleOrigins.size() == 1) {
            move.setOrigin(possibleOrigins.get(0));
        } else {
            int originColumn = retrieveOriginColumn(move.notation());
            move.setOrigin(possibleOrigins.stream()
                    .filter(c -> c.column() == originColumn)
                    .findFirst().orElse(possibleOrigins.get(0)));
        }
    }

    Coordinate tryToRetrieveOriginFromNotation(String notation) {
        String justCoordinates = notation.replace(ATTACK_SYMBOL, "").replace(CHECK_SYMBOL, "");
        if(justCoordinates.length() < 5) {
            return null;
        }
        return Coordinate.of(notation.substring(1, 3));
    }

    private int retrieveOriginColumn(String notation) {
        if (findOutPieceType(notation) == Pawn.class) {
            char columnChar = notation.substring(0, 1).toCharArray()[0];
            return Coordinate.column(columnChar);
        }
        if (justCoordinates(notation).length() < 4) return -1;
        char columnChar = notation.substring(1, 2).toCharArray()[0];
        return Coordinate.column(columnChar);
    }

    private String justCoordinates(String notation) {
        return notation.replace(ATTACK_SYMBOL, "").replace(CHECK_SYMBOL, "");
    }

    Coordinate retrieveDestination(String notation) {
        if(isResult(notation) || isCastling(notation)) {
            return null;
        }
        String justCoordinates = justCoordinates(notation);
        return of(justCoordinates.substring(justCoordinates.length() - 2));
    }

    Class findOutPieceType(String notation) {
        if (notation.length() == 2) return Pawn.class;
        if (notation.startsWith("N")) return Knight.class;
        if (notation.startsWith("R")) return Rook.class;
        if (notation.startsWith("Q")) return Queen.class;
        if (notation.startsWith("B")) return Bishop.class;
        if (notation.startsWith("K") || isCastling(notation) || isResult(notation)) return King.class;
        if (notation.startsWith("0-")) return King.class;
        if (notation.startsWith("1-")) return King.class;
        if (notation.startsWith("1/2")) return King.class;
        return Pawn.class;
    }

    boolean isAttack(String notation) {
        return notation.contains(ATTACK_SYMBOL);
    }

    private boolean isResult(String notation) {
        return isWhiteGiveUp(notation) ||
                isBlackGiveUp(notation) ||
                isDraw(notation);
    }

    private boolean isBlackGiveUp(String notation) {
        return "1-0".equals(notation);
    }

    private boolean isWhiteGiveUp(String notation) {
        return "0-1".equals(notation);
    }

    private boolean isDraw(String notation) {
        return "1/2-1/2".equals(notation);
    }

    boolean isCheck(String notation) {
        return notation.endsWith(CHECK_SYMBOL);
    }

    private boolean isSpecial(Move move) {
        String notation = move.notation();
        if (isResult(notation) || isCastling(notation) || isPromotion(notation)) {
            return true;
        }
        Coordinate destination = retrieveDestination(notation);
        Class pieceType = findOutPieceType(notation);
        return rules.isEnPassant(move, destination, pieceType);
    }

    private void populateEnPassant(Move move, PieceLocationAware locationAware) throws MoveException {
        move.setDestination(retrieveDestination(move.notation()));
        populateOrigin(move, locationAware);
        rules.setupEnPassant(move);
    }

    private boolean isPromotion(String notation) {
        if (findOutPieceType(notation) != Pawn.class) {
            return false;
        }
        int destinationRow = retrieveDestination(notation).row();
        return destinationRow == 0 || destinationRow == 7;
    }

    private boolean isCastling(String notation) {
        return isCastlingKingSide(notation) || isCastlingQueenSide(notation);
    }

    private boolean isCastlingKingSide(String notation) {
        return "O-O".equals(notation);
    }

    private boolean isCastlingQueenSide(String notation) {
        return "O-O-O".equals(notation);
    }

}
