package interview.chess.move;

/**
 * @author Borys Zora
 * @version 2020-03-05
 */
public enum MoveMotivation {

    ATTACK,
    PEACE

}
