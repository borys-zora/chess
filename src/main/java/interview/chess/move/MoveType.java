package interview.chess.move;

import java.util.List;

import static interview.chess.model.Color.BLACK;
import static interview.chess.model.Color.WHITE;
import static interview.chess.move.MoveMotivation.ATTACK;
import static interview.chess.move.MoveMotivation.PEACE;
import static interview.chess.move.Shift.of;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public enum MoveType {

    DIAGONAL(List.of(
            of(1, 1), of(2, 2), of(3, 3), of(4, 4), of(5, 5), of(6, 6), of(7, 7),
            of(-1, -1), of(-2, -2), of(-3, -3), of(-4, -4), of(-5, -5), of(-6, -6), of(-7, -7),
            of(1, -1), of(2, -2), of(3, -3), of(4, -4), of(5, -5), of(6, -6), of(7, -7),
            of(-1, 1), of(-2, 2), of(-3, 3), of(-4, 4), of(-5, 5), of(-6, 6), of(-7, 7))
    ),

    ONE_SQUARE_DIAGONAL(List.of(
            of(1, 1), of(-1, 1), of(1, -1), of(-1, -1)
    )),

    ONE_SQUARE_FORWARD_DIAGONAL_ATTACK_ONLY(List.of(
            of(1, 1).applyOnlyForColor(WHITE).changeMotivation(ATTACK),
            of(1, -1).applyOnlyForColor(WHITE).changeMotivation(ATTACK),
            of(-1, 1).applyOnlyForColor(BLACK).changeMotivation(ATTACK),
            of(-1, -1).applyOnlyForColor(BLACK).changeMotivation(ATTACK)
    )),

    /**
     * A vertical column of eight squares on the chessboard.
     */
    FILE(List.of(
            of(1, 0), of(2, 0), of(3, 0), of(4, 0), of(5, 0), of(6, 0), of(7, 0),
            of(-1, 0), of(-2, 0), of(-3, 0), of(-4, 0), of(-5, 0), of(-6, 0), of(-7, 0))
    ),

    ONE_SQUARE_FILE(List.of(
            of(1, 0), of(-1, 0)
    )),

    ONE_SQUARE_FORWARD_FILE(List.of(
            of(1, 0).applyOnlyForColor(WHITE).changeMotivation(PEACE),
            of(-1, 0).applyOnlyForColor(BLACK).changeMotivation(PEACE))
    ),

    TWO_SQUARE_FORWARD_FILE_ONLY_FIRST_MOVE(List.of(
            of(2, 0).applyOnlyForColor(WHITE).changeMotivation(PEACE),
            of(-2, 0).applyOnlyForColor(BLACK).changeMotivation(PEACE))
    ),

    /**
     * A horizontal row of eight squares on the chessboard.
     */
    RANK(List.of(
            of(0, 1), of(0, 2), of(0, 3), of(0, 4), of(0, 5), of(0, 6), of(0, 7),
            of(0, -1), of(0, -2), of(0, -3), of(0, -4), of(0, -5), of(0, -6), of(0, -7)
    )),

    ONE_SQUARE_RANK(List.of(
            of(0, 1), of(0, -1)
    )),

    /**
     * every cell that touches every cell that touches current
     * and from it take only opposite color to current cell
     */
    SECOND_SQUARE_NEIGHBORS_OPPOSITE_COLOR(List.of(
            of(2, 1), of(1, 2), of(-1, 2), of(-2, 1),
            of(-2, -1), of(-1, -2), of(1, -2), of(2, -1)
    )),

    SQUARE_UNDER_ATTACK_FORBIDDEN(List.of(
            of(0, 0)
    )),

    /**
     * When a pawn advances to the eighth rank, as a part of the move it is promoted and must be exchanged
     * for the player's choice of queen, rook, bishop, or knight of the same color. Usually, the pawn is chosen
     * to be promoted to a queen, but in some cases another piece is chosen; this is called underpromotion.
     */
    PROMOTION(List.of(
            of(1, 0).changeMotivation(PEACE).applyOnlyForColor(WHITE),
            of(1, -1).changeMotivation(ATTACK).applyOnlyForColor(WHITE),
            of(1, 1).changeMotivation(ATTACK).applyOnlyForColor(WHITE),
            of(-1, 0).changeMotivation(PEACE).applyOnlyForColor(BLACK),
            of(-1, -1).changeMotivation(ATTACK).applyOnlyForColor(BLACK),
            of(-1, 1).changeMotivation(ATTACK).applyOnlyForColor(BLACK)
    )),

    /**
     * When a pawn makes a two-step advance from its starting position and there is an opponent's pawn
     * on a square next to the destination square on an adjacent file, then the opponent's pawn can capture
     * it en passant ("in passing"), moving to the square the pawn passed over.
     * This can be done only on the very next turn; otherwise the right to do so is forfeited.
     */
    EN_PASSANT(
            List.of(of(1, 1).changeMotivation(ATTACK).applyOnlyForColor(WHITE),
                    of(1, -1).changeMotivation(ATTACK).applyOnlyForColor(WHITE),
                    of(-1, 1).changeMotivation(ATTACK).applyOnlyForColor(BLACK),
                    of(-1, -1).changeMotivation(ATTACK).applyOnlyForColor(BLACK))
    ),

    CASTLING_KING_SIDE(
            of(0, 2),
            List.of(of(0, 2), of(0, 1), of(0, 0)),
            List.of(of(0, 2), of(0, 1)),
            of(0, 3),
            of(0, -2)
    ),

    CASTLING_QUEEN_SIDE(
            of(0, -2),
            List.of(of(0, -2), of(0, -1), of(0, 0)),
            List.of(of(0, -3), of(0, -2), of(0, -1)),
            of(0, -4),
            of(0, 3)
    ),

    WHITE_GIVE_UP,

    BLACK_GIVE_UP,

    DRAW;

    private List<Shift> shifts;
    private List<Shift> attackSensitive;
    private List<Shift> emptyShifts;
    private Shift primaryItemShift;
    private Shift secondItemLocation;
    private Shift secondItemShift;

    MoveType() {}

    MoveType(List<Shift> shifts) {
        this.shifts = shifts;
    }

    MoveType(Shift primaryItemShift, List<Shift> attackSensitive, List<Shift> emptyShifts, Shift secondItemLocation,
             Shift secondItemShift) {
        this.primaryItemShift = primaryItemShift;
        this.attackSensitive = attackSensitive;
        this.emptyShifts = emptyShifts;
        this.secondItemLocation = secondItemLocation;
        this.secondItemShift = secondItemShift;
    }

    public List<Shift> shifts() {
        return shifts;
    }

    public List<Shift> attackSensitive() {
        return attackSensitive;
    }

    public Shift primaryItemShift() {
        return primaryItemShift;
    }

    public Shift secondItemLocation() {
        return secondItemLocation;
    }

    public Shift secondItemShift() {
        return secondItemShift;
    }

    public List<Shift> emptyShifts() {
        return emptyShifts;
    }
}
