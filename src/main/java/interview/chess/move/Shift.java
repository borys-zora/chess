package interview.chess.move;

import interview.chess.model.Color;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static interview.chess.model.Color.BLACK;
import static interview.chess.model.Color.WHITE;
import static interview.chess.move.MoveMotivation.ATTACK;
import static interview.chess.move.MoveMotivation.PEACE;
import static java.lang.Math.abs;
import static java.lang.Math.max;

/**
 * @author Borys Zora
 * @version 2020-03-05
 */
public class Shift {

    private int row;
    private int column;
    private List<Color> colors = List.of(WHITE, BLACK);
    private List<MoveMotivation> motivations = List.of(ATTACK, PEACE);

    private Shift(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public static Shift of(int row, int column) {
        return new Shift(row, column);
    }

    public static Shift of(Coordinate origin, Coordinate destination) {
        return of(destination.row() - origin.row(), destination.column() - origin.column());
    }

    public int row() {
        return row;
    }

    int column() {
        return column;
    }

    // not obvious: knight move logic only tolerant to obstacles and it is the pattern how to figure it out
    private boolean tolerantToObstacles() {
        return row != 0 && column != 0 && abs(row) != abs(column);
    }

    Shift applyOnlyForColor(Color color) {
        this.colors = List.of(color);
        return this;
    }

    Shift changeMotivation(MoveMotivation motivation) {
        this.motivations = List.of(motivation);
        return this;
    }

    boolean isApplicable(Color color) {
        return colors.contains(color);
    }

    public boolean isApplicable(MoveMotivation motivation) {
        return motivations.contains(motivation);
    }

    public List<Shift> obstacleSteps() {
        if (tolerantToObstacles()) {
            return Collections.emptyList();
        }
        int max = max(abs(row), abs(column));
        int rowStep = (row != 0) ? row/abs(row) : 0;
        int columnStep = (column != 0) ? column/abs(column) : 0;
        List<Shift> result = new ArrayList<>();
        for (int i = 1; i < max; i++) {
            result.add(Shift.of(i * rowStep, i * columnStep));
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Shift)) return false;

        Shift shift = (Shift) o;

        if (row != shift.row) return false;
        if (column != shift.column) return false;
        if (colors != null ? !colors.equals(shift.colors) : shift.colors != null) return false;
        return motivations != null ? motivations.equals(shift.motivations) : shift.motivations == null;
    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + column;
        result = 31 * result + (colors != null ? colors.hashCode() : 0);
        result = 31 * result + (motivations != null ? motivations.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Shift{" +
                "row=" + row +
                ", column=" + column +
                ", colors=" + colors +
                ", motivations=" + motivations +
                '}';
    }
}
