package interview.chess.move;

import interview.chess.model.Piece;
import java.util.Stack;

/**
 * @author Borys Zora
 * @version 2020-03-08
 */
public class MovesLog {

    private static final Stack<LogItem> ticks = new Stack<>();

    public static void log(Piece piece, String notation, Coordinate origin, Coordinate destination, MoveType moveType) {
        ticks.add(new LogItem(piece, notation, origin, destination, moveType));
    }

    public static LogItem rollback() {
        return ticks.pop();
    }

    public static Piece lastMovingPiece() {
        if (ticks.isEmpty()) return null;
        return ticks.peek().piece;
    }

    public static MoveType lastMovingType() {
        if (ticks.isEmpty()) return null;
        return ticks.peek().moveType;
    }

    public static class LogItem {
        int tick;
        Piece piece;
        String notation;
        Coordinate origin;
        Coordinate destination;
        MoveType moveType;

        LogItem(Piece piece, String notation, Coordinate origin, Coordinate destination, MoveType moveType) {
            this.tick = ticks.size() + 1;
            this.piece = piece;
            this.notation = notation;
            this.origin = origin;
            this.destination = destination;
            this.moveType = moveType;
        }

        public int tick() {
            return tick;
        }

        public Piece piece() {
            return piece;
        }

        public String notation() {
            return notation;
        }

        public Coordinate origin() {
            return origin;
        }

        public Coordinate destination() {
            return destination;
        }

        public MoveType type() {
            return moveType;
        }
    }
}
