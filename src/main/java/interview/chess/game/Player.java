package interview.chess.game;

import interview.chess.model.Color;
import interview.chess.move.Move;
import interview.chess.move.exception.MoveException;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public class Player {

    private Color color;
    private Board board;

    public Player(Board board, Color color) {
        this.board = board;
        this.color = color;
    }

    public void movePiece(Move move) throws MoveException {
        board.movePiece(move);
    }

    public Color color() {
        return color;
    }

}
