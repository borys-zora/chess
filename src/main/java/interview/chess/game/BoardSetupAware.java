package interview.chess.game;

/**
 * @author Borys Zora
 * @version 2020-03-08
 */
public interface BoardSetupAware {

    void setup();

}
