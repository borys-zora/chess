package interview.chess.game;

import interview.chess.model.*;

import static interview.chess.game.Board.BOARD_SIZE;
import static interview.chess.move.Coordinate.of;

/**
 * @author Borys Zora
 * @version 2020-03-08
 */
public class NewGameSetup implements BoardSetupAware {

    private final Board board;

    public NewGameSetup(Board board) {
        this.board = board;
    }

    @Override
    public void setup() {
        setupWhite();
        setupBlack();
    }

    private void setupWhite() {
        Color color = Color.WHITE;
        setupMainPieces(color, 0);
        setupPawns(color, 1);
    }

    private void setupBlack() {
        Color color = Color.BLACK;
        setupMainPieces(color, 7);
        setupPawns(color, 6);
    }

    private void setupMainPieces(Color color, int row) {
        add(new Rook(color, of(row, 0)));
        add(new Knight(color, of(row, 1)));
        add(new Bishop(color, of(row, 2)));
        add(new Queen(color, of(row, 3)));
        add(new King(color, of(row, 4)));
        add(new Bishop(color, of(row, 5)));
        add(new Knight(color, of(row, 6)));
        add(new Rook(color, of(row, 7)));
    }

    private void setupPawns(Color color, int row) {
        for(int column = 0; column < BOARD_SIZE; column++) {
            add(new Pawn(color, of(row, column)));
        }
    }

    private void add(Piece piece) {
        board.add(piece);
    }
}
