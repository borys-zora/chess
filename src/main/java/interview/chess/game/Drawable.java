package interview.chess.game;

/**
 * @author Borys Zora
 * @version 2020-03-05
 */
public interface Drawable {

    void doDraw(PieceMappingAware pieceMappingAware);

}
