package interview.chess.game;

import interview.chess.move.AlgebraicNotation;
import interview.chess.move.Move;
import interview.chess.model.Color;
import interview.chess.move.exception.MoveException;

import static java.lang.String.format;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public abstract class Game implements Drawable {

    private Board board;
    private Player whitePlayer;
    private Player blackPlayer;
    private Player currentPlayer;
    private AlgebraicNotation notation = new AlgebraicNotation();

    public Game() {
        this.board = new Board();
        new NewGameSetup(board).setup();
        whitePlayer = new Player(board, Color.WHITE);
        blackPlayer = new Player(board, Color.BLACK);
        currentPlayer = whitePlayer;
    }

    protected Player currentPlayer() {
        return currentPlayer;
    }

    private void switchPlayer() {
        currentPlayer = (currentPlayer == whitePlayer) ? blackPlayer : whitePlayer;
    }

    private boolean nextMove() {
        userMessage(format("\n%s's move: ", currentPlayer.color().name()));
        while(true) {
            try {
                Move move = getUsersMove();
                if (move.isResult()) {
                    userMessage(move.resultMessage());
                    return false;
                }
                currentPlayer.movePiece(move);
                return true;
            } catch (MoveException e) {
                userMessage(e.getMessage() + "\n");
                userMessage(format("%s's retry move: ", currentPlayer.color().name()));
            }
        }
    }

    protected Move getUsersMove() throws MoveException {
        return notation.parse(nextUserInput(), currentPlayer().color(), board);
    }

    protected abstract String nextUserInput();

    private void userMessage(String message) {
        System.out.print(message);
    }

    public void start() {
        doDraw(board);
        while (nextMove()) {
            switchPlayer();
            doDraw(board);
        }
    }
}
