package interview.chess.game;

import interview.chess.model.Piece;
import interview.chess.move.Coordinate;

import java.util.Map;

/**
 * @author Borys Zora
 * @version 2020-03-09
 */
public interface PieceMappingAware {

    Map<Coordinate, Piece> pieceMap();

}
