package interview.chess.game;

import interview.chess.model.*;
import interview.chess.move.*;
import interview.chess.move.exception.MoveValidationException;
import interview.chess.move.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static interview.chess.move.Coordinate.of;

/**
 * @author Borys Zora
 * @version 2020-03-04
 */
public class Board implements CastlingValidationResolver, SquareIsUnderAttackResolver, LocationResolver,
        KingProtectedResolver, PromotionResolver, PieceMappingAware, PieceLocationAware {

    static final int BOARD_SIZE = 8;
    private Map<Coordinate, Piece> pieceMap = new HashMap<>();

    void movePiece(Move move) throws MoveValidationException {
        validate(move);
        handleAttack(move);
        handleMoving(move);
        postMoveHandler(move);
    }

    void add(Piece piece) {
        pieceMap.put(piece.coordinate(), piece);
    }

    private void remove(Piece piece) {
        pieceMap.remove(piece.coordinate());
    }

    @Override
    public List<MoveType> allowedMoves(Coordinate origin) {
        return getPieceByCoordinate(origin).allowedMoves();
    }

    @Override
    public List<Coordinate> possibleOrigins(Class pieceType, Color color, Coordinate destination,
                                            MoveMotivation moveMotivation) {
        List<Piece> pieces = getPiecesByTypeAndColor(pieceType, color);
        return pieces.stream()
                .filter(p -> p.isValidMove(destination, moveMotivation))
                .filter(p -> tolerateObstacles(p, destination))
                .map(Piece::coordinate)
                .collect(Collectors.toList());
    }

    @Override
    public Piece getPieceByCoordinate(Coordinate coordinate) {
        return pieceMap.get(coordinate);
    }

    @Override
    public Piece findKing(Color color) {
        return getPiecesByTypeAndColor(King.class, color).get(0);
    }

    @Override
    public boolean squareIsNotAttackedByOpponent(Coordinate coordinate, Color opponentsColor) {
        return pieceMap.values().stream()
                .filter(p -> p.color() == opponentsColor)
                .filter(p -> p.possibleAttacks().contains(coordinate))
                .noneMatch(p -> tolerateObstacles(p, coordinate));
    }

    @Override
    public void swap(Piece piece, Piece promoted) {
        remove(piece);
        add(promoted);
    }

    @Override
    public Map<Coordinate, Piece> pieceMap() {
        return new HashMap<>(pieceMap);
    }

    private boolean tolerateObstacles(Piece piece, Coordinate destination) {
        return tolerateObstacles(piece.coordinate(), destination);
    }

    private boolean tolerateObstacles(Coordinate origin, Coordinate destination) {
        return Shift.of(origin, destination)
                .obstacleSteps().stream()
                .map(s -> getPieceByCoordinate(of(origin, s)))
                .allMatch(Objects::isNull);
    }

    private List<Piece> getPiecesByTypeAndColor(Class pieceType, Color color) {
        return pieceMap().values().stream()
                .filter(p -> p.getClass() == pieceType)
                .filter(p -> p.color() == color)
                .collect(Collectors.toList());
    }

    private void handleMoving(Move move) {
        Piece movingPiece = getPieceByCoordinate(move.origin());
        remove(movingPiece);
        movingPiece.move(move.destination());
        add(movingPiece);
        if (move.isSpecial()) {
            handleSpecialMove(move);
        }
        MovesLog.log(movingPiece, move.notation(), move.origin(), move.destination(), move.type());
    }

    private void handleSpecialMove(Move move) {
        if (move.isDoubleMove()) {
            handleMoving(move.secondMove());
        }
    }

    private void handleAttack(Move move) {
        if(move.isAttack()) {
            Piece pieceUnderAttack = getPieceByCoordinate(move.attackLocation());
            killPiece(pieceUnderAttack);
        }
    }

    private void killPiece(Piece piece) {
        remove(piece);
        piece.die();
    }

    private void validate(Move move) throws MoveValidationException {
        move.resolveConstraints(this);
        if ( ! move.isValid()) {
            throw new MoveValidationException(String.join("\n", move.constrainsMessages()));
        }
    }

    private void postMoveHandler(Move move) throws MoveValidationException {
        move.resolveAfterMoveConstraints(this);
        if ( ! move.isValidAfterMove()) {
            rollback(move);
            throw new MoveValidationException(String.join("\n", move.afterMoveConstrainsMessages()));
        }
    }

    private void rollback(Move move) {
        var logItem = MovesLog.rollback();
        pieceMap.remove(logItem.destination());
        pieceMap.put(logItem.origin(), logItem.piece());
        logItem.piece().rollback();
        if (move.isDoubleMove()) {
            rollback(move.secondMove());
        }
    }
}
