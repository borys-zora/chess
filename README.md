# chess

## specification & useful links

- [FIDE rules](https://handbook.fide.com/chapter/E012018)
- [chess on wiki](https://en.wikipedia.org/wiki/Chess)
- [games by players](http://www.pgnmentor.com/files.html#players)
- [unicode symbols examples](https://en.wikipedia.org/wiki/Chess_symbols_in_Unicode)
- [color for terminal](https://stackoverflow.com/questions/5762491/how-to-print-color-in-console-using-system-out-println)
- [algebraic notation](https://en.wikipedia.org/wiki/Algebraic_notation_\(chess\))

## how to use it

- install java 11 or later
- install maven
- use command `mvn clean package && java -jar target/chess.jar`
- use algebraic notation link above
        
Preview in IntelliJ IDEA        
![Alt text](chess-intellij-idea-white-mode.png "Preview in IntelliJ IDEA")

Mac OS default terminal basic theme
![Alt text](chess-mac-os-terminal-basic-theme.png "Mac OS default terminal basic theme")

## Spanish Opening example
first is WHITE's move and after space is BLACK's move

    1.e4 e5 2.Nf3 Nc6 3.Bb5 a6 4.Ba4 Nf6 5.O-O Be7 6.Re1 b5 7.Bb3 d6 8.c3 O-O
    9.h3 Bb7 10.d4 Re8 11.Nbd2 Bf8 12.a4 h6 13.d5 Nb8 14.c4 c6 15.axb5 axb5 16.Rxa8 Bxa8
    17.dxc6 b4 18.Ba4 Nxc6 19.Nf1 Qb8 20.g4 Rc8 21.Ng3 Nd8 22.g5 hxg5 23.Nxg5 Rxc4
    24.Bb3 Rd4 25.Qc2 Nd7 26.Be3 Nc5 27.Bxd4 exd4 28.Bd5 Be7 29.h4 Nde6 30.Bxe6 fxe6
    31.Qc4 d3 32.Nxe6 d2 33.Rd1 d5 34.exd5 Nxe6 35.Qe4 Bc5 36.Qxe6+ Kh8 37.Kg2 Qf4
    38.Qc8+ Kh7 39.Qxc5  1-0

![Alt text](Kasparov-Dorfman-1978.png "Mac OS default terminal basic theme")

## run existing games

    java -cp target/chess.jar:. \
            interview.chess.ui.AlgebraicNotationGameReader \
            Kasparov-Dorfman-1978.txt
    
    java -cp target/chess.jar:. \
            interview.chess.ui.AlgebraicNotationGameReader \
            src/test/resources/Kasparov-Suba-1982-round-12.txt
         

## known issues

in different consoles an appearance could be different, recommended console settings:

- white background
- black symbols color